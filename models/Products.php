<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "products".
 *
 * @property string $id
 * @property string $name
 * @property string $slug
 * @property string $price
 * @property string $old_price
 * @property string $body
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property integer $rotate360deg
 * @property integer $novelty
 * @property integer $bestseller
 * @property integer $in_stock
 * @property integer $made_in_ukraine
 * @property integer $enabled
 * @property string $created_at
 * @property string $updated_at
 */
class Products extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'products';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'slug', 'price'], 'required'],
            [['price', 'old_price'], 'number'],
            [['body'], 'string'],
            [['rotate360deg', 'novelty', 'bestseller', 'in_stock', 'made_in_ukraine', 'enabled'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'meta_title', 'meta_keywords', 'meta_description'], 'string', 'max' => 500],
            [['slug'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'slug' => 'Slug',
            'price' => 'Price',
            'old_price' => 'Old Price',
            'body' => 'Body',
            'meta_title' => 'Meta Title',
            'meta_keywords' => 'Meta Keywords',
            'meta_description' => 'Meta Description',
            'rotate360deg' => 'Rotate360deg',
            'novelty' => 'Novelty',
            'bestseller' => 'Bestseller',
            'in_stock' => 'In Stock',
            'made_in_ukraine' => 'Made In Ukraine',
            'enabled' => 'Enabled',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function getCategories() {
    	return $this->hasMany(ProductsCategories::className(), ['product_id' => 'id']);
    }
}
